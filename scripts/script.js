"use strict";

// 1. Цикли створенні для вирішення проблем з записом однотипних значень велику кількість разів.
//    Тобто, якщо нам потрібно буде записати якесь значення, наприклад, 100 разів, то ми використаємо цикли.
//    При цьому ми значно полегшимо свою роботу і спростимо читання нашого коду для сабе та інших.
// 2. Цикл for частіше використовується коли нам потрібно виконати більше умов для написання функції. 
//    While - в більш скороченому вигляді.
// 3. Явне перетворення - це коли ми хочемо перетворити значення одного типу в будь-який інший.
//    В джава скрипті значення можуть бути сконвертовані автоматично з будь-якими іншими типами - це називається неявне перетворення.


let num = prompt('Enter number');
for (let i = 0; i <= num; i++) {
    if (num < 5) {
        console.log('Sorry, no numbers');
        break;
    }
    if (i % 5 === 0) {
    console.log(i);
    }
}